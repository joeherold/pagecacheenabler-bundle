# Fix broken Contao 4 Page Cache

This bundle is a quick fix for the broken Contao Page Cache. Running a productive Website without a working Page-Cache results in very high TTFB.


In Symfony 3.4.4 the Behaviour of the AbstractSessionListener Class was changed:
https://github.com/symfony/http-kernel/blob/v3.4.4/EventListener/AbstractSessionListener.php#L53

Change Commit can be seen here: https://github.com/symfony/http-kernel/commit/6622fe25afe6a2c594189cd72ab83c49014b57df#diff-db3c5d9b0dc8cbb67671806266bef598

In case, you find any issues, feel free to inform: https://bitbucket.org/joeherold/pagecacheenabler-bundle/issues

The problem is discussed here: https://github.com/contao/core-bundle/issues/1246