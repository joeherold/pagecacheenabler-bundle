<?php

/*
 * This file is part of Pliigo.
 *
 * Copyright (c) 2018 Johannes Pichler
 *
 * @license LGPL-3.0+
 */

namespace Pliigo\PliigoPageCacheEnabler\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Adds the bundle services to the container.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class PageCacheEnablerExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $mergedConfig, ContainerBuilder $container)
    {

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');
        $loader->load('listener.yml');
        $loader->load('routing.yml');
    }
}
