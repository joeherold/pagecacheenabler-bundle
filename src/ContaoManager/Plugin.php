<?php


/*
 * This file is part of Pliigo.
 *
 * Copyright (c) 2018 Johannes Pichler
 *
 * @license LGPL-3.0+
 */

namespace Pliigo\PliigoPageCacheEnabler\ContaoManager;


use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Pliigo\PliigoPageCacheEnabler\PliigoPageCacheEnablerBundle;


/**
 * Plugin for the Contao Manager.
 *
 * @author Johannes Pichler <https://github.com/joeherold>
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(PliigoPageCacheEnablerBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
                ->setReplace(['pliigo-page-cache-enabler']),
        ];
    }
}
